<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Product Filter And Search</title>
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500&display=swap"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="../css/filter.css" />
  </head>
  <body>
  
    <div class="wrapper">
      <div id="search-container">
        <input
          type="search"
          id="search-input"
          placeholder="Search product name here.."
        />
        <button id="search">Search</button>
      </div>
      <div id="buttons">
        <input type="checkbox" id='all' name="checkboxAll" onclick="filterProduct()">All
        <input type="checkbox" id='mobile' name="checkboxMobile" onclick="filterProduct()">Mobile
        <input type="checkbox" id='tablets' name="checkboxTablets" onclick="filterProduct()">Tablets
        <input type="checkbox" id='airConditioners' name="checkboxAirConditioners" onclick="filterProduct()">Air conditioners
        </button>
      </div>
      <div id="products"></div>
    </div>
    <script src="filter.js"></script>
  </body>
</html>

<?php
 require_once 'DAO.php';
 $dao = new DAO();
$products = $dao->getLastNProducts(50);

?>

<script> 

var productsList = <?php echo json_encode($products) ?>;
for (let products of productsList) {

  if(products.status=='true'){
    let card = document.createElement("div");
   
    // let imgContainer = document.createElement("div");
    // imgContainer.classList.add("image-container");

    // let image = document.createElement("img");
    // image.setAttribute("src", i.image);
    // imgContainer.appendChild(image);
    // card.appendChild(imgContainer);
    
    let container = document.createElement("div");
    container.classList.add("container");
    
    let type = document.createElement("h3");
    type.classList.add("product-type");
    type.innerText = products.type.toUpperCase();
    container.appendChild(type);

    let name = document.createElement("h4");
    name.classList.add("product-name");
    name.innerText = products.name.toUpperCase();
    container.appendChild(name);

    let status= document.createElement("h6");
    status.classList.add("product-status");
    status.innerText = products.status.toUpperCase();
    container.appendChild(status);

    let price = document.createElement("h6");
    price.classList.add("product-price");
    price.innerText = products.price.toUpperCase();
    container.appendChild(price);
    // let price = document.createElement("h6");
    // price.innerText = "$" + i.price;
    // container.appendChild(price);
  
    card.appendChild(container);
    document.getElementById("products").appendChild(card);
  }
}
</script>
