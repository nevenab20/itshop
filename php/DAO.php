<?php
require_once 'database.php';

class DAO {
	private $db;

	// za 2. nacin resenja
	//private $INSERTOSOBA = "INSERT INTO osoba (ime, prezime, JMBG, vremeUpisa) VALUES (?, ?, ?, CURRENT_TIMESTAMP)";
	//private $DELETEOSOBA = "DELETE  FROM osoba WHERE idosoba = ?";
	//private $SELECTBYID = "SELECT * FROM osoba WHERE idosoba = ?";	
	private $GETLASTNPRODUCTS = "SELECT * FROM products ORDER BY id ASC LIMIT ?";
	//private $USERS="SELECT * FROM users ";
	private $INSERTUSER = "INSERT INTO users(type,username,password) values ('kupac',?,?)";
	private $SELECT_USER_BY_USERNAME_AND_PASSWORD = "SELECT * FROM users join atributes on users.atribute_id = atributes.id where users.username = ? and users.password = ?";		public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function getLastNProducts($n)
	{
		
		$statement = $this->db->prepare($this->GETLASTNPRODUCTS);
		$statement->bindValue(1, $n, PDO::PARAM_INT);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}
	public function selectUserByUsernameAndPassword($username, $password)
	{
		
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME_AND_PASSWORD);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
	
	public function insertUser($username, $password)
	{
		
		$statement = $this->db->prepare($this->INSERTUSER);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		
		$statement->execute();
	}
/*
	public function insertOsoba($ime, $prezime, $JMBG)
	{
		
		$statement = $this->db->prepare($this->INSERTOSOBA);
		$statement->bindValue(1, $ime);
		$statement->bindValue(2, $prezime);
		$statement->bindValue(3, $JMBG);
		
		$statement->execute();
	}

	public function deleteOsoba($idosoba)
	{
		
		$statement = $this->db->prepare($this->DELETEOSOBA);
		$statement->bindValue(1, $idosoba);
		
		$statement->execute();
	}

	public function getOsobaById($idosoba)
	{
		
		$statement = $this->db->prepare($this->SELECTBYID);
		$statement->bindValue(1, $idosoba);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
	*/
}

?>
