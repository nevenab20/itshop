<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../css/contact-page.css">
</head>
<body>
    <h1>Drop Us a Line</h1>

    <p>Have a question or comment?</p>

    <select id="customer-service">
        <option >Customer service</option>
        <option>Customer service</option>
    </select>
    <input type="text" onchange="validateEmail()" name="email" id="email" placeholder="your@email.com">
    <br>
    <input type="file" name="file" id="file">
    <br>
    <textarea name="text-area" id="text-area" cols="50" rows="10" placeholder="How can we help?">
    </textarea><br>
    <input onclick="send()" type="submit" name="post-comment" value="Post Comment" id="button">

    <div id='email-validity'></div>
<script src="submit.js"></script>
</body>
</html>

<script>
   function  validateEmail() {
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onload = function() {
  const myObj = JSON.parse(this.responseText);
  //debugger;
  document.getElementById("email-validity").innerHTML = myObj.valid ? '' : 'BAD EMAIL!';
}
xmlhttp.open("GET", "validate-email.php?email=" + document.getElementById('email').value);
xmlhttp.send();
    }
</script>