<?php
require_once 'DAO.php';
$action = isset($_REQUEST["action"])? $_REQUEST["action"] : ""; 

if ($_SERVER['REQUEST_METHOD']=="POST"){
    if ($action == 'LOGIN') {
        $username = isset($_POST["username"])? test_input($_POST["username"]) : ""; //provera da li su setovani podaci
        $password = isset($_POST["password"])? test_input($_POST["password"]) : ""; //provera da li su setovani podaci
        $dao = new DAO();
        $user = $dao->selectUserByUsernameAndPassword($username,$password);
        if($user){
            session_start();
            $_SESSION['user'] = $user;
            $_SESSION['last-active'] = time();
          
            if (isset($_POST['rememberMe'])) {
                $userCookie = array('username' => $_POST['username'], 'password' => $_POST['password']);
            
                setcookie("userJSON", json_encode($userCookie), time() + 20, "/");
            }

            include_once $user['type'].'.php';
        }else{
            $msg = "Pogresni parametri za logovanje!!!";
            include_once 'index.php';
        }
        
    }
}
function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>