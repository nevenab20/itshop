<?php

 require_once 'DAO.php';
 $dao = new DAO();
 $products=$dao->getLastNProducts(5);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<table border=1 width="80%" align="center">
        <tr>
            <th>ID</th>
            <th>TYPE</th>
            <th>NAME</th>
            <th>STATUS</th>
            <th>PRICE</th>
        </tr>
<?php foreach ($products as $pom ){?>
        <tr>
            <td><?= $pom['id'] ?></td>
            <td><?= $pom['type'] ?></td>
            <td><?= $pom['name'] ?></td>
            <td><?= $pom['status'] ?></td>
            <td><?= $pom['price'] ?></td>
        </tr>
        <?php }?>
</body>
</html>